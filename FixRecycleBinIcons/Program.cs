﻿using Microsoft.Win32;

namespace FixRecycleBinIcons
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            using (RegistryKey regKey = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Explorer\CLSID\{645FF040-5081-101B-9F08-00AA002F954E}\DefaultIcon", true))
            {
                string @default = regKey.GetValue(null).ToString();
                string empty = regKey.GetValue("empty").ToString();
                string full = regKey.GetValue("full").ToString();

                if (!@default.Contains(","))
                    regKey.SetValue(null, @default + ",0");
                else
                {
                    int start = @default.IndexOf(',');
                    @default = @default.Substring(0, start);
                    regKey.SetValue(null, @default + ",0");
                }

                if (!empty.Contains(","))
                    regKey.SetValue("empty", empty + ",0");
                else
                {
                    int start = empty.IndexOf(',');
                    empty = empty.Substring(0, start);
                    regKey.SetValue("empty", empty + ",0");
                }

                if (!full.Contains(","))
                    regKey.SetValue("full", full + ",0");
                else
                {
                    int start = full.IndexOf(',');
                    full = full.Substring(0, start);
                    regKey.SetValue("full", full + ",0");
                }
            }
        }
    }
}
